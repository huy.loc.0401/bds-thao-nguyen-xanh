import React from "react";
import Grid from "@mui/material/Grid";
import TextField from "@mui/material/TextField";
import { useHistory } from "react-router-dom";
import Button from "@mui/material/Button";

export const Search = () => {
  const [text, setText] = React.useState("");
  const history = useHistory();
  return (
    <Grid container justifyContent={"center"}>
      <TextField
        onChange={(e) => {
          setText(e.target.value);
        }}
        placeholder="Tìm kiếm theo khu vực (theo tỉnh, huyện hoặc xã)"
        style={{ width: "100%" }}
      />
      <Button
        style={{ marginTop: "5px" }}
        onClick={() => {
          history.push(`/search/${text}`);
        }}
        variant="contained"
      >
        Tìm kiếm
      </Button>
    </Grid>
  );
};
export default Search;
