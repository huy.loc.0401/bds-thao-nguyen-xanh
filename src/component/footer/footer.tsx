/* eslint-disable jsx-a11y/iframe-has-title */
/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import Grid from "@mui/material/Grid";
export const Footer = () => {
  return (
    <Grid container style={{ background: "whitesmoke", marginTop: "100px" }}>
      <Grid container item>
        <Grid container item xs={9} justifyContent={"space-between"}>
          <Grid item style={{ display: "flex" }}>
            <img
              style={{ width: "55px", height: "60px" }}
              src="https://img.icons8.com/ios/50/000000/apple-phone.png"
              alt="icon"
            />
            <div style={{ marginLeft: "5px" }}>
              <p style={{ lineHeight: "25px" }}>Hotline</p>

              <p style={{ fontWeight: 600 }}>0379834725</p>
            </div>
          </Grid>

          <Grid item style={{ display: "flex" }}>
            <img
              style={{ width: "55px", height: "60px" }}
              src="https://img.icons8.com/ios/50/000000/headset.png"
            />
            <div style={{ marginLeft: "5px" }}>
              <p style={{ lineHeight: "25px" }}>Hỗ trợ khách hàng</p>
              <p style={{ fontWeight: 600 }}>hotroTNX@gmail.com@gmail.com</p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Grid item style={{ width: "100%" }}>
        <iframe
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8816.207173922601!2d108.334113182037!3d11.885783673788515!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31716be7f4fd2917%3A0x11d5ee12bd301893!2zTcOqIExpbmgsIEzDom0gSMOgLCBMw6JtIMSQ4buTbmcsIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1635415579516!5m2!1svi!2s"
          width="100%"
          height="450"
          loading="lazy"
        ></iframe>
      </Grid>
      <Grid container>
        <Grid container item>
          <Grid item>
            <h3 style={{ color: "#36D7B7", fontWeight: 700 }}>
              CÔNG TY BẤT ĐỘNG SẢN THẢO NGUYÊN XANH
            </h3>
          </Grid>
          <Grid item container justifyContent="space-between">
            <Grid>
              <div style={{ display: "flex" }}>
                <img
                  style={{ width: "25px", height: "25px", marginTop: "10px" }}
                  src="https://img.icons8.com/fluency-systems-filled/64/000000/cottage.png"
                />
                <p style={{ marginLeft: "5px" }}>
                  Xóm 3, Thôn 3, Xã Mê Linh, Huyện Lâm Hà, Tỉnh Lâm Đồng
                </p>
              </div>
              <div style={{ display: "flex" }}>
                <img
                  style={{ width: "25px", height: "25px", marginTop: "10px" }}
                  src="https://img.icons8.com/ios-glyphs/64/000000/phone.png"
                />
                <p style={{ marginLeft: "5px" }}>0379834725</p>
              </div>
            </Grid>

            <Grid item style={{ display: "flex", flexDirection: "column" }}>
              <p
                style={{
                  lineHeight: "25px",
                  marginLeft: "5px",
                  fontSize: "Roboto Regular",
                }}
              >
                Copyright © 2021 - 2050 Công Ty BĐS Thảo Nguyên Xanh
              </p>
              <p
                style={{
                  lineHeight: "25px",
                  marginLeft: "5px",
                  fontSize: "Roboto Regular",
                }}
              >
                Mã số doanh nghiệp số 5801435682 do Sở KHĐT Tỉnh Lâm Đồng cấp
                lần đầu ngày 18/06/2020
              </p>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Footer;
