import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";

const useStyle = makeStyles({
  describe: {
    display: "flex",
    flexDirection: "column",
    fontFamily: "Nunito Bold",
    fontSize: "14px",
    color: "#004e7f",
    fontWeight: 600,
  },
  priceDetail: {
    display: "flex",
    justifyContent: "space-between",
    color: "#2c2c2c",
    fontSize: "14px",
    fontWeight: 600,
  },
  item: {
    padding: "10px",
    borderRadius: "10px",
    background: "white",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 2px 10px 5px #ccc",
    },
  },
  state: {
    fontFamily: "Nunito Bold",
    fontSize: "14px",
    color: "#2c2c2c",
    fontWeight: 500,
  },
});
export interface ItemProps {
  img?: string;
  title: string;
  area: string;
  price: string;
  location: string;
  des?: string;
}
export const ItemRealEstate = ({
  title,
  area,
  price,
  location,
  img,
  des,
}: ItemProps) => {
  const classes = useStyle();
  return (
    <Grid container className={classes.item}>
      <Grid item style={{ width: "100%" }}>
        <img style={{ width: "100%", height: "200px" }} src={img} alt="#" />
      </Grid>
      <Grid item>
        <div className={classes.describe}>
          <p> {title}</p>
        </div>
        <div className={classes.describe}>
          <p> {des}</p>
        </div>
        <div className={classes.priceDetail}>
          <p>DT {area} m2</p>
          <p style={{ marginLeft: "10px" }}> Giá {price} tỷ </p>
        </div>
        <div className={classes.state}>
          <p>{location}</p>
        </div>
      </Grid>
    </Grid>
  );
};
export default ItemRealEstate;
