/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";

const useStyle = makeStyles({
  describe: {
    display: "flex",
    fontFamily: "Nunito Bold",
    fontSize: "14px",
    lineHeight: "20px",
    textTransform: "uppercase",
    color: "#961b12",
    fontWeight: 600,
    marginLeft: "10px",
  },
  priceDetail: {
    display: "flex",
    justifyContent: "space-between",
    color: "#2c2c2c",
    fontSize: "14px",
    fontWeight: 600,
    marginLeft: "10px",
  },
  item: {
    padding: "10px",
    borderRadius: "10px",
    background: "white",
    cursor: "pointer",
    "&:hover": {
      background: "whitesmoke",
    },
  },
  state: {
    fontFamily: "Roboto Regular",
    fontSize: "14px",
    color: "#505050",
    fontWeight: 500,
    marginLeft: "10px",
  },
});
export interface ItemProps {
  hinh1?: unknown;
  title?: string;
  area?: string;
  price?: string;
  location?: string;
  description?: string;
  img?: string;
}
export const ItemLand = ({
  title,
  area,
  price,
  location,
  description,
  img,
}: ItemProps) => {
  const classes = useStyle();
  return (
    <Grid container className={classes.item}>
      <Grid item xs={4} style={{ width: "100%" }}>
        <img style={{ width: "100%", height: "100%" }} src={img} alt="#" />
      </Grid>
      <Grid item xs={7}>
        <div className={classes.describe}>
          <span style={{ width: "25px", height: "15px" }}>
            <img
              src="https://img.icons8.com/fluency/48/000000/star.png"
              style={{ width: "100%", height: "100%" }}
            />
          </span>
          <span> {title}</span>
        </div>
        <div className={classes.priceDetail}>
          <p>DT {area} m2</p>
          <p> Giá {price} tỷ </p>
          <p>{location}</p>
        </div>
        <div className={classes.state}>{description}</div>
      </Grid>
    </Grid>
  );
};
export default ItemLand;
