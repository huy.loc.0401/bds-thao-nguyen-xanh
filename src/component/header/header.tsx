import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";
import logo from "../../assets/images/logo.jpg";
import { Link } from "react-router-dom";

const useStyles = makeStyles({
  item: {
    fontFamily: "Nunito Bold",
    fontSize: "20px",
    fontWeight: 600,
    textDecoration: "none",
    color: "#2c2c2c",
    marginTop: "20px",
    "&:hover": {
      color: "#36D7B7",
    },
  },
  header: {
    position: "fixed",
    top: "0",
    background: "whitesmoke",
    zIndex: 9999999999,
  },
});

export const Header = () => {
  const classes = useStyles();
  return (
    <Grid
      className={classes.header}
      container
      justifyContent="space-between"
      style={{ height: "80px", background: "white" }}
    >
      <Grid item xs={1} style={{ height: "100%" }}>
        <img src={logo} alt="Logo" style={{ width: "100%", height: "100%" }} />
      </Grid>
      <Grid
        container
        justifyContent="space-around"
        item
        xs={11}
        style={{ paddingTop: "25px" }}
      >
        <Grid item>
          <Link className={classes.item} to="/">
            Trang chủ
          </Link>
        </Grid>

        <Grid item>
          <Link className={classes.item} to="/gioi-thieu">
            Giới thiệu
          </Link>
        </Grid>

        <Grid item>
          <Link className={classes.item} to="/dat-du-an">
            Đất dự án
          </Link>
        </Grid>

        <Grid item>
          <Link className={classes.item} to="/dat-mat-duong">
            Đất mặt đường
          </Link>
        </Grid>

        <Grid item>
          <Link className={classes.item} to="/dat-vuon">
            Đất vườn
          </Link>
        </Grid>
      </Grid>
    </Grid>
  );
};

export default Header;
