/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import Grid from "@mui/material/Grid";
import { ItemRealEstate } from "../../component/Item-real-estate/item";
import { makeStyles } from "@mui/styles";
import SearchComponent from "../../component/search";
import axios from "axios";
import { useParams } from "react-router-dom";
const useStyle = makeStyles({
  body: {
    width: "80%",
    margin: "auto",
  },
  title: {
    fontFamily: "Nunito Bold",
    color: "#2c2c2c",
    lineHeight: "32px",
  },
  Extend: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  icon: {
    display: "flex",
    alignItems: "center",
    fontFamily: "Nunito Bold",
    color: "#2c2c2c",
    border: "1px solid #ccc",
    background: "#ffffff",
    borderRadius: "4px",
    padding: "10px",
    paddingLeft: "25px",
    paddingRight: "25px",
    cursor: "pointer",
    "&:hover": {
      background: "#ccc",
    },
    "& p": {
      lineHeight: "25px",
    },
    "& svg": {
      marginLeft: "5px",
    },
  },
});
type Item = {
  hinh1?: string;
  Id: number;
  name: string;
  area: string;
  price: string;
  xa: string;
  huyen: string;
  tinh: string;
  ImgArray: string;
  description: string;
};

export const SearchPage = () => {
  const [data, setData] = React.useState<Item[]>([]);
  const [numberDatahot, SetNumberDatahot] = React.useState(12);
  const params: any = useParams();
  const paramText = params.text.trim();
  console.log("params", paramText);

  const body = { q: paramText };
  console.log("paramText", paramText);

  React.useEffect(() => {
    axios
      .post("https://api.bdsxanhlamdong.com.vn/common/", body)

      .then((res) => {
        console.log(res.data.data);
        setData(res.data.data);
      })
      .catch((error) => console.log(error));
  }, []);

  // sản phẩm của bạn

  const classes = useStyle();
  return (
    <div style={{ marginTop: "86px", width: "100%" }}>
      {/* <div style={{ width: "100%" }}>
        <img style={{ width: "100%" }} src={BackGround} alt="" />
      </div> */}
      <div className={classes.body}>
        <SearchComponent />
        <Grid container>
          <Grid item>
            <Grid item>
              <h2 className={classes.title}>Bất động sản nổi bật</h2>
            </Grid>
            <Grid xs={6}></Grid>
          </Grid>
          <Grid item container>
            {data.map((item, index) => {
              if (index < numberDatahot)
                return (
                  <Grid
                    key={index}
                    item
                    xs={12}
                    sm={3}
                    md={3}
                    style={{ padding: "5px" }}
                  >
                    <a
                      href={`https://bdsxanhlamdong.com.vn/detailpage/${item.Id}`}
                      style={{ textDecoration: "none" }}
                    >
                      <ItemRealEstate
                        img={`https://api.bdsxanhlamdong.com.vn/static/image/${
                          JSON.parse(item.ImgArray)[0]
                        }`}
                        title={item.name}
                        des={item.description}
                        location={`Xã ${item.xa} huyện ${item.huyen} tỉnh ${item.tinh}`}
                        price={item.price}
                        area={item.area}
                      />
                    </a>
                  </Grid>
                );
            })}
          </Grid>
          <Grid item className={classes.Extend}>
            <div
              className={classes.icon}
              onClick={() => SetNumberDatahot(numberDatahot + 8)}
            >
              <p>Mở rộng</p>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-chevron-down"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
                />
              </svg>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
export default SearchPage;
