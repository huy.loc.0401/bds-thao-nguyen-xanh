/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";
import TableContainer from "@mui/material/TableContainer";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import axios from "axios";
import { useParams } from "react-router-dom";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-skyblue.min.css";
import ItemRealEstate from "../../component/Item-real-estate/item";

const useStyle = makeStyles({
  pageDetail: {
    margin: "auto",
    width: "80% !important",
    marginTop: "80px",
    "& .splide__play": {
      display: "none",
    },
    "& .splide__pause": {
      display: "none",
    },
    "& button:nth-child(2)": {
      left: "80%",
    },
  },
  title: {
    fontFamily: "Nunito Bold",
    fontSize: "24px",
    color: "#2c2c2c",
    fontWeight: 600,
  },
  state: {
    fontFamily: "Nunito Bold",
    fontSize: "18px",
    color: "#2c2c2c",
    display: "block",
    width: "100%",
  },
  feature: {
    fontFamily: "Nunito Bold",
    fontSize: "20px",
    color: "#2c2c2c",
    fontWeight: 600,
    display: "block",
    width: "100%",
  },
  filter: {
    marginTop: "10px",
    width: "100%",
    fontFamily: "Nunito Bold",
    border: "solid 2px whitesmoke",
    marginLeft: "15px",
    borderRadius: "4px",
    "& h4": {
      fontSize: "15px",
      cursor: "pointer",
      fontWeight: 500,
      marginLeft: "15px",
    },
    "& h2": {
      fontSize: "18px",
      fontWeight: 600,
      marginLeft: "15px",
    },
  },
});
type Item = {
  Id: string;
  name: string;
  area: string;
  price: string;
  xa: string;
  huyen: string;
  tinh: string;
  thon: string;
  description: string; //
  duong?: string; // đường bê tông 5 m
  matTien?: string; // mặt tiền
  direction?: string; /// hướng nhà
  phapLy?: string; // sổ đỏ....
  thoCu?: string; // thổ cư
  ImgArray: string;
};
const host_be = "https://api.bdsxanhlamdong.com.vn";

export const DetailPage = () => {
  const params: any = useParams();
  console.log(params.productID);

  const [data, setData] = React.useState<Item>();
  const [dataHot, setDataHot] = React.useState<Item[]>([]);
  React.useEffect(() => {
    axios
      .get(`${host_be}/product/detail/${params.productID}`)
      .then((res) => {
        setData(res.data.data[0]);
        //console.log("XXXXXXXXX", res.data.data[0]);
        // const ARRAY = JSON.parse(data['ImgArray']);
        // console.log('ARRAYARRAY', ARRAY);
      })
      .catch((error) => console.log(error));
  }, []);
  React.useEffect(() => {
    axios
      .get(`${host_be}/product/hot`)
      .then((res) => {
        console.log(res.data.data);
        if (res.data.status == 200) {
          setDataHot(res.data.data);
        } else {
        }
      })
      .catch((error) => console.log(error));
  }, []);
  const classes = useStyle();
  return (
    <Grid container className={classes.pageDetail}>
      <Grid container>
        <Grid item container style={{ margin: "auto", width: "85%" }}>
          <Splide
            options={{
              type: "loop",
              gap: "1rem",
              autoplay: false,
              arrows: "slider",
            }}
            hasSliderWrapper
            hasAutoplayControls
            hasAutoplayProgress
          >
            {data
              ? JSON.parse(data["ImgArray"]).map((item: any, index: any) => {
                  return (
                    <SplideSlide key={index}>
                      <img
                        alt=""
                        //src={item}
                        src={`https://api.bdsxanhlamdong.com.vn/static/image/${item}`}
                        style={{
                          width: "85%",
                          height: "500px",
                        }}
                      />
                    </SplideSlide>
                  );
                })
              : ""}
          </Splide>

          <p className={classes.title}>{data?.name}</p>
          <p className={classes.state} style={{ lineHeight: "45px" }}>
            <span>Giá: {data?.price} tỷ</span>
            <span style={{ marginLeft: "15px" }}>DT: {data?.area} m2</span>
          </p>
          <p className={classes.feature}>Thông tin mô tả</p>
          <p>{data?.description}</p>
          <TableContainer component={Paper} style={{ width: "85%" }}>
            <p className={classes.feature}>Đặc điểm bất động sản</p>
            <Table sx={{ minWidth: 650 }} aria-label="simple table">
              <TableBody>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Địa chỉ
                  </TableCell>
                  <TableCell align="center">{data?.xa}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Mặt tiền
                  </TableCell>
                  <TableCell align="center">{data?.matTien}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Đường vào
                  </TableCell>
                  <TableCell align="center">{data?.duong}</TableCell>
                </TableRow>

                <TableRow>
                  <TableCell component="th" scope="row">
                    Pháp lý
                  </TableCell>
                  <TableCell align="center">{data?.phapLy}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell component="th" scope="row">
                    Thổ cư
                  </TableCell>
                  <TableCell align="center">{data?.thoCu}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <h1 className={"hotline"}>Zalo/Hotline: 0988499215</h1>
      <h1 className={"zalo"}>
        {" "}
        <a href="https://zalo.me/0379834725">Zalo/Hotline: 0379834725</a>{" "}
      </h1>
      <h1 style={{ marginTop: "50px" }}>Một số sản phẩm nổi bật</h1>
      <Grid item container>
        {dataHot.map((item, index) => {
          if (index < 8)
            return (
              <Grid
                key={index}
                item
                xs={12}
                sm={3}
                md={3}
                style={{ padding: "5px" }}
              >
                <a
                  href={"/detailpage/" + item.Id}
                  style={{ textDecoration: "none" }}
                >
                  <ItemRealEstate
                    img={`https://api.bdsxanhlamdong.com.vn/static/image/${
                      JSON.parse(item.ImgArray)[0]
                    }`}
                    title={item.name}
                    location={`Xã ${item.xa} huyện ${item.huyen} tỉnh ${item.tinh}`}
                    price={item.price}
                    area={item.area}
                  />
                </a>
              </Grid>
            );
        })}
      </Grid>
    </Grid>
  );
};
export default DetailPage;
