/* eslint-disable eqeqeq */
import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";
import axios from "axios";
import { useForm, SubmitHandler } from "react-hook-form";
import { useHistory } from "react-router-dom";
import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import Paper from "@mui/material/Paper";
import Button from "@mui/material/Button";
import Dialog from "@mui/material/Dialog";
import DialogActions from "@mui/material/DialogActions";
import DialogTitle from "@mui/material/DialogTitle";

const useStyle = makeStyles({
  field: {
    "& select": {
      display: "block",
      width: "100%",
      marginBottom: "15px",
      height: "30px",
    },
    "& input": {
      display: "block",
      width: "100%",
      marginTop: "15px",
      height: "30px",
    },
    "& label": {
      fontSize: "20px",
    },
  },
});

const Districts = [
  {
    value: 1,
    name: "Đà Lạt",
  },
  {
    value: 2,
    name: "Bảo Lộc",
  },
  {
    value: 3,
    name: "Lâm Hà",
  },
];

const Wards = [
  [
    {
      value: "1",
      name: "Phường 1",
    },
    {
      value: "2",
      name: "Phường 2",
    },
    {
      value: "3",
      name: "Phường 3",
    },
    {
      value: "4",
      name: "Phường 4",
    },
    {
      value: "5",
      name: "Phường 5",
    },
    {
      value: "6",
      name: "Phường 6",
    },
    {
      value: "7",
      name: "Phường 7",
    },
    {
      value: "8",
      name: "Phường 8",
    },
    {
      value: "9",
      name: "Phường 9",
    },
    {
      value: "10",
      name: "Phường 10",
    },
    {
      value: "11",
      name: "Phường 11",
    },
    {
      value: "12",
      name: "Phường 12",
    },
    {
      value: "13",
      name: "Xã Tà Nung",
    },
    {
      value: "14",
      name: "Xã Trạm Hành",
    },
    {
      value: "15",
      name: "Xã Xuân Thọ",
    },
    {
      value: "16",
      name: "Xã Xuân Trường",
    },
  ],
  [
    {
      value: "1",
      name: "Phường 1",
    },
    {
      value: "2",
      name: "Phường 2",
    },
    {
      value: "3",
      name: "Phường Blao",
    },
    {
      value: "4",
      name: "Phường Lộc Phát",
    },
    {
      value: "5",
      name: "Phường Lộc Sơn",
    },
    {
      value: "6",
      name: "Phường Lộc Tiến",
    },
    {
      value: "7",
      name: "Xã Đại Lào",
    },
    {
      value: "8",
      name: "Xã DamBri",
    },
    {
      value: "9",
      name: "Xã Lộc Châu",
    },
    {
      value: "10",
      name: "Xã Lộc Nga",
    },
    {
      value: "11",
      name: "Xã Lộc Thanh",
    },
  ],
  [
    {
      value: "1",
      name: "Thị trấn Nam Ban",
    },
    {
      value: "2",
      name: "Thị trấn Đinh Văn",
    },
    {
      value: "3",
      name: "Xã Tân Hà",
    },
    {
      value: "4",
      name: "Xã Nam Hà",
    },
    {
      value: "5",
      name: "Xã Mê Linh",
    },
    {
      value: "6",
      name: "Xã Tân Thanh",
    },
    {
      value: "7",
      name: "Xã Hoài Đức",
    },
    {
      value: "8",
      name: "Xã Phúc Thọ",
    },
    {
      value: "9",
      name: "Xã Đan Phượng",
    },
    {
      value: "10",
      name: "Xã Đạ Đờn",
    },
    {
      value: "11",
      name: "Xã Đông Thanh",
    },
    {
      value: "12",
      name: "Xã Gia Lâm",
    },
    {
      value: "13",
      name: "Xã Phi Liêng",
    },
    {
      value: "14",
      name: "Xã Phi Tô",
    },
    {
      value: "15",
      name: "Xã Rô Men",
    },
    {
      value: "16",
      name: "Xã Liêng Srônh",
    },
    {
      value: "17",
      name: "Xã Liên Hà",
    },
  ],
];
interface IFormInput {
  tinh: string;
  huyen: string;
  xa: string;
  name: string;
  description: string;
  area: string;
  price: string;
  duong: string;
  phapLy: string;
  thoCu: string;
  // active: string,
  matTien: string;
  category: string;
  hotProduct: string;
  //forYouProduc: string;
}
type Item = {
  Id: string;
  name: string;
  area: string;
  price: string;
  xa: string;
  huyen: string;
  tinh: string;
  description: string; //
  duong?: string; // đường bê tông 5 m
  matTien?: string; // mặt tiền
  direction?: string; /// hướng nhà
  phapLy?: string; // sổ đỏ....
  thoCu?: string; // thổ cư
  ImgArray: string;
};
export const Admin = () => {
  const classes = useStyle();
  const [dataWard, setDataWard] = React.useState<any[]>(Wards[0]);
  const { register, handleSubmit } = useForm<IFormInput>();
  const [openModal, setOpenModal] = React.useState(false);
  const [select, SetSelect] = React.useState<any>();
  const [del, setDel] = React.useState<any>(0);
  const [data, setData] = React.useState<Item[]>([]);
  const history = useHistory();

  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    var formData = new FormData();
    console.log("myFilessssss", data);
    for (var [key, value] of Object.entries(data)) {
      formData.append(`${key}`, `${value}`);
    }
    formData.append("myFiles", select[0]);
    formData.append("myFiles", select[1]);
    formData.append("myFiles", select[2]);
    formData.append("myFiles", select[3]);
    formData.append("myFiles", select[4]);
    console.log("formData", formData);

    const config = {
      headers: {
        "content-type": "multipart/form-data",
      },
    };

    axios
      .post(
        "https://api.bdsxanhlamdong.com.vn/admin/product/create",
        formData,
        config
      )
      .then((res) => {
        if (res.data.status == 200) {
          alert("Tạo thành công");
        } else {
          alert("Tạo sai vui lòng tạo lại!");
        }
      })
      .catch((error) => console.log(error));
  };
  const handleDelete = () => {
    axios
      .delete(`https://api.bdsxanhlamdong.com.vn/admin/product/delete/${del}`)
      .then((res) => {
        if (res.data.status == 200) {
          window.location.reload();
        } else {
          alert("Xoá không thành công");
        }
      })
      .catch((error) => console.log(error));
    setOpenModal(false);
    //;
  };
  const handelChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    Districts.forEach((element) => {
      if (element.name == e.target.value) {
        setDataWard(Wards[element.value - 1]);
      }
    });
  };
  const handleClickOpenModal = (id:any) => {
    setDel(id);
    setOpenModal(true);
  };

  const handleClose = () => {
    setOpenModal(false);
  };

  React.useEffect(() => {
    axios
      .get(`https://api.bdsxanhlamdong.com.vn/product`)
      .then((res) => {
        console.log(res.data);
        setData(res.data.data);
      })
      .catch((error) => console.log(error));
  }, []);
  const handleClick = () => {
    localStorage.clear();
    history.push("/");
  };
  return (
    <Grid className={classes.field} container style={{ marginTop: "100px" }}>
      <Grid item xs={3}>
        <Grid item>
          <button
            onClick={() => handleClick()}
            style={{
              backgroundColor: "red",
              borderRadius: "5px",
              padding: "5px",
              color: "#0d167e",
            }}
          >
            Đăng xuất
          </button>
        </Grid>
        <Grid item style={{ marginTop: "30px" }}>
          <h2>Đăng sản phẩm bất động sản</h2>
          <form onSubmit={handleSubmit(onSubmit)}>
            <label htmlFor="">Tỉnh</label>
            <select {...register("tinh")}>
              <option value={"Lâm Đồng"}>Lâm Đồng</option>
            </select>
            <label htmlFor="">Huyện</label>
            <select
              {...register("huyen")}
              onChange={(e) => {
                handelChange(e);
              }}
            >
              {Districts.map((item, index) => {
                return (
                  <option key={index} value={item.name}>
                    {item.name}
                  </option>
                );
              })}
            </select>
            <label htmlFor="">Xã</label>

            <select {...register("xa")}>
              {dataWard.map((item, index) => {
                return (
                  <option key={index} value={item.name}>
                    {item.name}
                  </option>
                );
              })}
            </select>
            <label htmlFor="">Loại</label>
            <select {...register("category")}>
              <option value={0}>Đất dự án</option>
              <option value={1}>Đất vườn</option>
              <option value={2}>Đất mặt đường</option>
            </select>
            <label htmlFor="hotProduct">Sản phẩm hot</label>
            <select id="hotProduct" {...register("hotProduct")}>
              <option value={0}>Không</option>
              <option value={1}>Có</option>
            </select>
            {/* <label htmlFor="">Sản phẩm dành cho bạn</label> */}
            {/* <select {...register("forYouProduc")}>
            <option value={0}>Không</option>
            <option value={1}>Có</option>
          </select> */}
            <input {...register("name")} type="text" placeholder="Tiêu đề" />
            <input
              {...register("description")}
              type="text"
              placeholder="Mô tả"
            />
            <input {...register("area")} type="text" placeholder="Diện tích" />
            <input {...register("price")} type="text" placeholder="Giá" />
            <input {...register("duong")} type="text" placeholder="Đường vào" />
            <input {...register("phapLy")} type="text" placeholder="Pháp lý" />
            <input {...register("thoCu")} type="text" placeholder="Thổ cư" />
            <input
              {...register("matTien")}
              type="text"
              placeholder="Mặt tiền"
            />
            <input
              onChange={(e) => {
                console.log("e.target.files", e.target.files);
                // SetFileImg(e.target.files[0]);
                // console.log('fileImgfileImg', fileImg);

                let arr = [];
                for (let i = 0; i < e.target.files.length; i++) {
                  arr.push(e.target.files[i]);
                }
                SetSelect(arr);
              }}
              type="file"
              name="myFiles"
              multiple
            />
            <input
              type="submit"
              style={{
                color: "#e95959",
                backgroundColor: "#1818d7",
              }}
            />
          </form>
        </Grid>
      </Grid>
      <Grid item xs={9}>
        <h1>Danh sách sản phẩm</h1>
        <TableContainer component={Paper}>
          <Table aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Hình</TableCell>
                <TableCell>Tên</TableCell>
                <TableCell>Vị Trí</TableCell>
                <TableCell>Mô tả</TableCell>
                <TableCell>Diện tích</TableCell>
                <TableCell>Giá</TableCell>
                <TableCell>Thổ cư</TableCell>
                <TableCell>Đường vào</TableCell>
                <TableCell>Hành động</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {data.map((row) => (
                <TableRow
                  //key={row.Id}
                  sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
                >
                  <TableCell>
                    <img
                      style={{ width: "200px", height: "200px" }}
                      src={`https://api.bdsxanhlamdong.com.vn/static/image/${
                        JSON.parse(row.ImgArray)[0]
                      }`}
                      alt=""
                    />
                  </TableCell>
                  <TableCell component="th" scope="row">
                    {row.name}
                  </TableCell>
                  <TableCell>{`${row.xa} ${row.huyen} ${row.tinh}`}</TableCell>
                  <TableCell>{row.description}</TableCell>
                  <TableCell>{`${row.area} m2`}</TableCell>
                  <TableCell>{`${row.price} tỷ`}</TableCell>
                  <TableCell>{`${row.thoCu} m2`}</TableCell>
                  <TableCell>{`${row.duong} m`}</TableCell>
                  <TableCell>
                    <Button variant="outlined" onClick={()=>{handleClickOpenModal(row.Id)}}>
                      Xoá
                    </Button>
                    <Dialog open={openModal} onClose={handleClose}>
                      <DialogTitle style={{ textAlign: "center" }}>
                        Bạn muốn xoá
                      </DialogTitle>

                      <DialogActions>
                        <Button
                          onClick={() => {
                            handleDelete();
                           
                          }}
                        >
                          Đồng ý
                        </Button>
                        <Button onClick={handleClose}>Huỷ</Button>
                      </DialogActions>
                    </Dialog>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </Grid>
    </Grid>
  );
};
export default Admin;
