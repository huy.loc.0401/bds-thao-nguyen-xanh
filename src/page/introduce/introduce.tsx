import React from "react";

export const Introduce = () => {
  return (
    <div style={{ marginTop: "100px", width: "80%" }}>
      <h1>
        Công ty bất động sản Thảo Nguyên Xanh khu vực huyện Lâm Hà tỉnh Lâm Đồng
      </h1>
      <h3>
        Công ty chúng tôi là một trong những công ty uy tín hàng đầu khu vực
        tỉnh Lâm Đồng về mua bán bất đông sản. Công ty chúng tôi mua bán trao
        đổi các bất động sản về dự án, đất vườn, đất mặt đường cũng như các bất
        động sản khác.
      </h3>
      <h3>
        Công ty chúng tôi rất vinh dự được phục vụ quý khách. Liên hệ với chúng
        tôi qua hotline hoặc zalo:0379834725
      </h3>
    </div>
  );
};
export default Introduce;
