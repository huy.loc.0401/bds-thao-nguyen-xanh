/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import TextField from "@mui/material/TextField";
import { useHistory } from "react-router-dom";
export const Login = () => {
  const [acc, setAcc] = React.useState("");
  const [pass, setPass] = React.useState("");
  const history = useHistory();
  React.useEffect(() => {

    if (acc === "admin" && pass === "12345@@@") {
      localStorage.setItem("value", "1");
      history.push("/adminPage");
      window.location.reload();
    } else {
      localStorage.removeItem("value");
    }
  }, [acc, pass]);
  return (
    <div style={{ margin : "auto" ,marginTop: "100px" }}>
      <h2>Đăng Nhập</h2>
      <TextField
        id="outlined-textarea"
        label="Tài khoản"
        placeholder="Tài khoản"
        onChange={(e) => setAcc(e.target.value)}
      />
      <TextField
        type="password"
        id="outlined-textarea"
        label="Mật khẩu"
        placeholder="Mật khẩu"
        onChange={(e) => setPass(e.target.value)}
      />
    </div>
  );
};
