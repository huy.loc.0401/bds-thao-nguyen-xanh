/* eslint-disable array-callback-return */
import React from "react";
import Grid from "@mui/material/Grid";
import { makeStyles } from "@mui/styles";
import axios from "axios";
import ItemByCate from "../../component/Item-real-estate/itemByCategory";
const host_be = "https://api.bdsxanhlamdong.com.vn";

const useStyle = makeStyles({
  filter: {
    marginTop: "10px",
    width: "100%",
    fontFamily: "Nunito Bold",
    border: "solid 2px whitesmoke",
    marginLeft: "15px",
    borderRadius: "4px",
    "& h4": {
      fontSize: "15px",
      cursor: "pointer",
      fontWeight: 500,
      marginLeft: "15px",
    },
    "& h2": {
      fontSize: "18px",
      fontWeight: 600,
      marginLeft: "15px",
    },
  },
  total: {
    width: "80% !important",
    margin: "auto",
  },
  Extend: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  icon: {
    display: "flex",
    alignItems: "center",
    fontFamily: "Nunito Bold",
    color: "#2c2c2c",
    border: "1px solid #ccc",
    background: "#ffffff",
    borderRadius: "4px",
    padding: "10px",
    paddingLeft: "25px",
    paddingRight: "25px",
    cursor: "pointer",
    "&:hover": {
      background: "#ccc",
    },
    "& p": {
      lineHeight: "25px",
    },
    "& svg": {
      marginLeft: "5px",
    },
  },
});

type Item = {
  hinh1?: string;
  Id: number;
  name: string;
  area: string;
  price: string;
  xa: string;
  huyen: string;
  tinh: string;
  ImgArray: string;
  description: string;
};
export const Garden = () => {
  const classes = useStyle();
  const [data, setData] = React.useState<Item[]>([]);
  const [dataInit, setDatainit] = React.useState<Item[]>([]);
  React.useEffect(() => {
    axios
      .get(`${host_be}/product/dat-vuon`)
      .then((res) => {
        setData(res.data.data);
        setDatainit(res.data.data);
      })
      .catch((error) => console.log(error));
  }, []);

  const handleClickPrice = (a: number, b?: number) => {
  
    const data1 = dataInit.filter(item=>
      Number(item.price)>=a && Number(item.price)<=b
    );
    setData(data1);
  };

  const handleClickPriceArea = (a: number, b?: number) => {
  
    const data1 = dataInit.filter(item=>
      Number(item.area)>=a && Number(item.area)<=b
    );
    setData(data1);
  };
  const [numberDatahot, SetNumberDatahot] = React.useState(5);
  return (
    <Grid className={classes.total} container style={{ marginTop: "100px" }}>
      <Grid container item spacing={5}>
        <Grid item xs={8} spacing={5}>
          <h2>Đất vườn</h2>
          {data.map((item, index) => {
            if (index < numberDatahot)
              return (
                <Grid key={index}>
                  <a
                    href={"/detailpage/" + item.Id}
                    style={{ textDecoration: "none" }}
                  >
                    <ItemByCate
                      img={`https://api.bdsxanhlamdong.com.vn/static/image/${
                        JSON.parse(item.ImgArray)[0]
                      }`}
                      des={item.description}
                      title={item.name}
                      location={`${item.xa} ${item.huyen} ${item.tinh}`}
                      price={item.price}
                      area={item.area}
                    />
                  </a>
                </Grid>
              );
          })}
          <h1 className={"hotline"}>Zalo/Hotline: 0988499215</h1>
          <h1 className={"zalo"}>
            {" "}
            <a href="https://zalo.me/0379834725">Zalo/Hotline: 0379834725</a>{" "}
          </h1>
          <Grid item className={classes.Extend}>
            <div
              className={classes.icon}
              onClick={() => SetNumberDatahot(numberDatahot + 8)}
            >
              <p>Mở rộng</p>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-chevron-down"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
                />
              </svg>
            </div>
          </Grid>
        </Grid>
        <Grid item xs={4}>
          <div className={classes.filter}>
            <h2>Lọc theo khoảng giá</h2>
            <h4 onClick={() => handleClickPrice(0, 0.5)}>Nhỏ hơn 500 triệu</h4>
            <h4 onClick={() => handleClickPrice(0.5, 0.8)}>500 - 800 triệu</h4>
            <h4 onClick={() => handleClickPrice(0.8, 1)}>800 triệu - 1 tỷ</h4>
            <h4 onClick={() => handleClickPrice(1, 2)}>1 - 2 tỷ</h4>
            <h4 onClick={() => handleClickPrice(2, 3)}>2 - 3 tỷ</h4>
            <h4 onClick={() => handleClickPrice(3, 5)}>3 - 5 tỷ</h4>
            <h4 onClick={() => handleClickPrice(5, 7)}>5 - 7 tỷ</h4>
            <h4 onClick={() => handleClickPrice(7, 10)}>7 - 10 tỷ</h4>
            <h4 onClick={() => handleClickPrice(10, 20)}>10 - 20 tỷ</h4>
            <h4 onClick={() => handleClickPrice(20)}>Lớn hơn 20 tỷ</h4>
          </div>
          <div className={classes.filter}>
            <h2>Lọc theo diện tích</h2>
            <h4 onClick={() => handleClickPriceArea(0, 200)}>Nhỏ hơn 200 m2</h4>
            <h4 onClick={() => handleClickPriceArea(200, 500)}>200 - 500 m2</h4>
            <h4 onClick={() => handleClickPriceArea(500, 1000)}>
              500 m2 - 1000 m2
            </h4>
            <h4 onClick={() => handleClickPriceArea(1000, 5000)}>
              1000 m2 - 5000 m2
            </h4>
            <h4 onClick={() => handleClickPriceArea(5000, 10000)}>
              5000 m2 - 1ha
            </h4>
            <h4 onClick={() => handleClickPriceArea(10000, 50000)}>1 - 5 ha</h4>
            <h4 onClick={() => handleClickPriceArea(50000, 100000)}>
              5 - 10 ha
            </h4>
            <h4 onClick={() => handleClickPriceArea(100000, 200000)}>
              10 - 20 ha
            </h4>
            <h4 onClick={() => handleClickPriceArea(200000)}> Lớn hơn 20 ha</h4>
          </div>
        </Grid>
      </Grid>
    </Grid>
  );
};
export default Garden;
