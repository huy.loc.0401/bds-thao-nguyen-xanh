/* eslint-disable eqeqeq */
/* eslint-disable array-callback-return */
import React from "react";
import Grid from "@mui/material/Grid";
import { ItemRealEstate } from "../../component/Item-real-estate/item";
import { makeStyles } from "@mui/styles";
import SearchComponent from "../../component/search";
import BackGround from "../../assets/images/background.jpg";
import demo1 from "../../assets/images/demo1.jpg";
import axios from "axios";
import { Splide, SplideSlide } from "@splidejs/react-splide";
import "@splidejs/splide/dist/css/themes/splide-skyblue.min.css";
import "./index.css";
const host_be = "https://api.bdsxanhlamdong.com.vn";

const useStyle = makeStyles({
  body: {
    width: "80%",
    margin: "auto",
  },
  title: {
    fontFamily: "Nunito Bold",
    color: "#2c2c2c",
    lineHeight: "32px",
  },
  Extend: {
    width: "100%",
    display: "flex",
    justifyContent: "center",
  },
  icon: {
    display: "flex",
    alignItems: "center",
    fontFamily: "Nunito Bold",
    color: "#2c2c2c",
    border: "1px solid #ccc",
    background: "#ffffff",
    borderRadius: "4px",
    padding: "10px",
    paddingLeft: "25px",
    paddingRight: "25px",
    cursor: "pointer",
    "&:hover": {
      background: "#ccc",
    },
    "& p": {
      lineHeight: "25px",
    },
    "& svg": {
      marginLeft: "5px",
    },
  },
  slide: {
    marginTop: "86px",
    width: "100%",
    "& .splide__play": {
      display: "none",
    },
    "& .splide__pause": {
      display: "none",
    },
  },
});
type Item = {
  hinh1?: string;
  Id: number;
  name: string;
  area: string;
  price: string;
  xa: string;
  huyen: string;
  tinh: string;
  ImgArray: string;
  description: string;
};

export const Home = () => {
  const [dataHot, setDataHot] = React.useState<Item[]>([]);
  const [numberDatahot, SetNumberDatahot] = React.useState(8);
  const [numberMydata, SetNumberMydata] = React.useState(8);
  const [myData, setMyData] = React.useState<Item[]>([]);

  // sản phẩm hot

  React.useEffect(() => {
    axios
      .get(`${host_be}/product/hot`)
      .then((res) => {
        console.log(res.data.data);
        if (res.data.status == 200) {
          setDataHot(res.data.data);
        } else {
        }
      })
      .catch((error) => console.log(error));
  }, []);

  // sản phẩm của bạn
  React.useEffect(() => {
    axios
      .get(`${host_be}/product`)
      .then((res) => {
        console.log(res.data);
        if (res.data.status == 200) {
          setMyData(res.data.data);
        } else {
        }
      })
      .catch((error) => console.log(error));
  }, []);

  const classes = useStyle();
  return (
    <div className={classes.slide} style={{ marginTop: "86px" }}>
      <Splide
        options={{
          type: "loop",
          gap: "1rem",
          autoplay: true,
          arrows: "slider",
        }}
        hasSliderWrapper
        hasAutoplayControls
        hasAutoplayProgress
      >
        <SplideSlide>
          <img style={{ width: "100%" }} src={BackGround} alt="" />
        </SplideSlide>
        <SplideSlide>
          <img style={{ width: "100%" }} src={demo1} alt="" />
        </SplideSlide>
        {/* <SplideSlide>
          <img style={{ width: "100%" }} src={BackGround} alt="" />
        </SplideSlide> */}
      </Splide>
  
      <h1 className={"hotline"}>Zalo/Hotline: 0988499215</h1>
      <h1 className={"zalo"}>
        {" "}
        <a href="https://zalo.me/0379834725">Zalo/Hotline: 0379834725</a>{" "}
      </h1>
      <div className={classes.body}>
        <SearchComponent />
        <Grid container>
          <Grid item>
            <Grid item>
              <h2 className={classes.title}>Bất động sản nổi bật</h2>
            </Grid>
          </Grid>
          <Grid item container>
            {dataHot.map((item, index) => {
              if (index < numberDatahot)
                return (
                  <Grid
                    key={index}
                    item
                    xs={12}
                    sm={3}
                    md={3}
                    style={{ padding: "5px" }}
                  >
                    <a
                      href={"/detailpage/" + item.Id}
                      style={{ textDecoration: "none" }}
                    >
                      <ItemRealEstate
                        img={`https://api.bdsxanhlamdong.com.vn/static/image/${
                          JSON.parse(item.ImgArray)[0]
                        }`}
                        des={item.description}
                        title={item.name}
                        location={`${item.xa} ${item.huyen} ${item.tinh}`}
                        price={item.price}
                        area={item.area}
                      />
                    </a>
                  </Grid>
                );
            })}
          </Grid>
          <Grid item className={classes.Extend}>
            <div
              className={classes.icon}
              onClick={() => SetNumberDatahot(numberDatahot + 8)}
            >
              <p>Mở rộng</p>
              <svg
                xmlns="https://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-chevron-down"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
                />
              </svg>
            </div>
          </Grid>
        </Grid>
        <Grid container>
          <Grid item>
            <Grid item>
              <h2 className={classes.title}>Bất động sản dành cho bạn</h2>
            </Grid>
          </Grid>
          <Grid item container>
            {myData.map((item, index) => {
              if (index < numberMydata)
                return (
                  <Grid
                    key={index}
                    item
                    xs={12}
                    sm={3}
                    md={3}
                    style={{ padding: "5px" }}
                  >
                    <a
                      href={"/detailpage/" + item.Id}
                      style={{ textDecoration: "none" }}
                    >
                      <ItemRealEstate
                        //https://api.bdsxanhlamdong.com.vn/static/image/
                        img={`https://api.bdsxanhlamdong.com.vn/static/image/${
                          JSON.parse(item.ImgArray)[0]
                        }`}
                        title={item.name}
                        location={`${item.xa} ${item.huyen} ${item.tinh}`}
                        price={item.price}
                        area={item.area}
                      />
                    </a>
                  </Grid>
                );
            })}
          </Grid>
          <Grid item className={classes.Extend}>
            <div
              className={classes.icon}
              onClick={() => SetNumberMydata(numberMydata + 8)}
            >
              <p>Mở rộng</p>
              <svg
                xmlns="https://www.w3.org/2000/svg"
                width="16"
                height="16"
                fill="currentColor"
                className="bi bi-chevron-down"
                viewBox="0 0 16 16"
              >
                <path
                  fillRule="evenodd"
                  d="M1.646 4.646a.5.5 0 0 1 .708 0L8 10.293l5.646-5.647a.5.5 0 0 1 .708.708l-6 6a.5.5 0 0 1-.708 0l-6-6a.5.5 0 0 1 0-.708z"
                />
              </svg>
            </div>
          </Grid>
        </Grid>
      </div>
    </div>
  );
};
export default Home;
