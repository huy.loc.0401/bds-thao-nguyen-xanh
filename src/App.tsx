/* eslint-disable react-hooks/exhaustive-deps */
import React from "react";
import Grid from "@mui/material/Grid";
import Footer from "./component/footer/footer";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Login } from "./page/login/login";
import HomePage from "./page/homepage";
import { IntroducePage } from "./page/introduce";
import { ProjectLandPage } from "./page/project-land";
import Header from "./component/header/header";
import DetailPage from "./page/detailpage/detailpage";
import { SurfaceLandPage } from "./page/surface-land";
import { GardenPage } from "./page/garden";
import { AdminPage } from "./page/admin";
import SearchPage from "./page/searchPage/searchPage";

const Public = () => (
  <h1
    style={{
      marginTop: "200px",
      textAlign: "center",
      width: "100%",
      color: "red",
    }}
  >
    Page error
  </h1>
);
function App() {
  const [a, setA] = React.useState(0);
  React.useEffect(() => {
    if (localStorage.getItem("value") === "1") {
      setA(1);
    } else setA(0);
  }, [localStorage.getItem("value")]);
  return (
    <Router>
      <Grid container justifyContent="space-between" style={{ height: "80px" }}>
        <Header />
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <Route exact path="/gioi-thieu">
            <IntroducePage />
          </Route>
          <Route exact path="/dat-du-an">
            <ProjectLandPage />
          </Route>
          <Route exact path="/dat-vuon">
            <GardenPage />
          </Route>
          <Route exact path="/dat-mat-duong">
            <SurfaceLandPage />
          </Route>
          <Route exact path="/detailpage/:productID">
            <DetailPage />
          </Route>
          <Route exact path="/search/:text">
            <SearchPage />
          </Route>
          {a === 1 && (
            <Route exact path="/adminPage">
              <AdminPage />
            </Route>
          )}
          {a === 0 && (
            <Route exact path="/admin">
              <Login />
            </Route>
          )}

          <Route path="*" exact={true}>
            <Public />
          </Route>
        </Switch>
        <Footer />
      </Grid>
    </Router>
  );
}

export default App;
